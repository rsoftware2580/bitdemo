package com.velocity.admin.ecommerce;

/*Author name - Raghvendra Mane & Sanket Dhomase
 * 
 */
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Scanner;

public class BillingStmt extends StockManager {

	public void getBillDetails() {
		String total = null;
		int cartId = 0;
		try {
			Database db = new Database();
			Connection connection = db.getConDetails();
			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery("select * from cart");// for geting bill

			System.out.println("///////////////Billing Invoice(Rs.)///////////////");
			System.out.println("  Product Name  " + "     Qty " + " Price ");
			System.out.println("----------------------------------------");

			while (rs.next()) {

				cartId = rs.getInt(1);// getting Cartid from cart
				PreparedStatement preparedStatement = connection
						.prepareStatement("update cart set TotalPrice = CProPrice*PurchesQty where CartId = ?");
				preparedStatement.setInt(1, cartId);// set cartId
				preparedStatement.execute();// for multiple product price in cart
				System.out.println(rs.getString(2) + rs.getString(4) + " * " + rs.getInt(3));// print product name and
																								// sub
				// total(product Price * quantity)
			}

			// System.out.println(cartId);
			PreparedStatement ps1 = connection.prepareStatement("select sum(TotalPrice) from cart;");
			ResultSet rSet1 = ps1.executeQuery();// getting all sub total sum
			while (rSet1.next()) {
				total = rSet1.getString("sum(TotalPrice)");
			}

			System.out.println("----------------------------------------");
			System.out.println("             Total Amt. : " + total + "/-");

			System.out.println("\nBuy confirm?...(Y/N)");
			Scanner scanner = new Scanner(System.in);
			String input = scanner.next();
			for (int j = 0; j < input.length(); j++) {
				if (input.charAt(j) == 'Y' || input.charAt(j) == 'y') {
					System.out.println("1. Online Payment" + "\n2. COD");
					System.out.println("\n------------------------>Thank-You<-------------------------");

				} else if (input.charAt(j) == 'N' || input.charAt(j) == 'n') {

					System.out.println("Oops!Your order is canceled...");
					System.out.println("\n------------------------>Thank-You<-------------------------");

				}
			}
			System.out.println("\n\n                                  @all rights are reserved");
			FillStock fillStock = new FillStock();
			fillStock.getStockManeger();// Fill Cart data into Stock

			TruncateCart.getRefershCart();

		} catch (Exception e) {
			e.getMessage();
		}
	}
}

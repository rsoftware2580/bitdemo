package com.velocity.admin.ecommerce;
/* Author name - Raghvendr mane & Sanket Dhomase & Vijay Chavan & Shubham Chambhare
 * */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Database {

	public Connection getConDetails() throws ClassNotFoundException, SQLException {
		Connection connection = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/ecomerce", "root", "Ram@8564");
		} catch (Exception e) {
			e.printStackTrace();
		}

		return connection;
	}
}

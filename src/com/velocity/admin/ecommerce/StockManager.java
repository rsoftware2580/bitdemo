package com.velocity.admin.ecommerce;

/* Author name - Raghvendr mane & Sanket Dhomase & Vijay Chavan & Shubham Chambhare
 * 
 */
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Scanner;

public class StockManager {

	public static void getProductAvablity(int Nid) {

		try {
			Database db = new Database();
			Connection connection = db.getConDetails();
			Scanner scanner = new Scanner(System.in);

			PreparedStatement ps = connection.prepareStatement("SELECT * FROM Product WHERE ProId=?");
			ps.setInt(1, Nid);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {

				System.out.println("Entre Quantity(Remaining-" + rs.getInt(5) + ") :");
				int qty = scanner.nextInt();
				if (qty <= rs.getInt(5)) {
					int i = rs.getInt(5) - qty;
					PreparedStatement preparedStatement = connection
							.prepareStatement("update product set ProQty = ? where ProId = ?");
					preparedStatement.setInt(1, i);
					preparedStatement.setInt(2, Nid);
					preparedStatement.execute();
					CartProduct cartProduct = new CartProduct();
					cartProduct.getCartDetailes(Nid, qty);
				} else if (qty > rs.getInt(5)) {
					System.out.println("Out of Stock!");
					getProductAvablity(Nid);
				}

			}

		} catch (Exception e) {
			e.getMessage();
		}
	}
}

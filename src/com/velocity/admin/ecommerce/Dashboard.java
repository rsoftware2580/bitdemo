package com.velocity.admin.ecommerce;

/*Author name - Vijay Chavan & Sanket Dhomase
 * 
 */
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class Dashboard {

	static Scanner scanner = null;

	public static void getListDetails() throws ClassNotFoundException, SQLException {
		// String s1 = null;

		Database db = new Database();
		Connection connection = db.getConDetails();
		Statement statement = connection.createStatement();
		ResultSet rSet = statement.executeQuery("select * from Product order by ProName asc");
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>Our Product List here<<<<<<<<<<<<<<<<<<<<<<");
		System.out.println("  Product Name     " + "    Product Price   " + "  Product Code" + "  Description ");
		System.out.println("------------------------------------------------------------------");
		while (rSet.next()) {
			System.out.println(rSet.getString(2) + ": " + rSet.getInt(4) + "       " + "          " + rSet.getInt(1)
					+ "        " + rSet.getString(3));

		}

		getProductPurches();
	}

	public static void getProductPurches() throws ClassNotFoundException, SQLException {
		scanner = new Scanner(System.in);
		System.out.println("\nEnter Purchase Product Code...");
		int id = scanner.nextInt();
		StockManager.getProductAvablity(id);

	}

	public void getLoginDetails(String name, String pass) throws SQLException, ClassNotFoundException {

		try {

			Database db = new Database();
			Connection connection = db.getConDetails();

			Statement statement = connection.createStatement();
			String sql = "Select * from user where Username = '" + name + "' and Password = '" + pass + "'";
			ResultSet resultSet = statement.executeQuery(sql);
			if (resultSet.next()) {
				System.out.println("@Login Successfull...");
				getListDetails();
			} else {
				System.out.println("Wrong User!");
				getScannerClass();
			}
		} catch (Exception e) {
			e.getMessage();
		}
	}

	public static void getScannerClass() throws ClassNotFoundException, SQLException {
		scanner = new Scanner(System.in);
		System.out.println("Enter Username :");
		String a = scanner.next();
		System.out.println("Enter Password :");
		String b = scanner.next();
		Dashboard dashboard = new Dashboard();
		dashboard.getLoginDetails(a, b);
	}

	public static void main(String[] args) throws SQLException, ClassNotFoundException {
		System.out.println("---------------Welcome To Online Shopping Portal--------------");
		TruncateCart.getRefershCart();
		getScannerClass();
	}
}

package com.velocity.admin.ecommerce;

/* Author name - Vijay Chavan & Shubham Chambhare
 * 
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;
import java.sql.ResultSet;

public class CartProduct extends StockManager {

	public void getCartDetailes(int code, int quantity) throws ClassNotFoundException, SQLException {

		try {
			Database db = new Database();
			Connection connection = db.getConDetails();
			PreparedStatement ps = connection.prepareStatement("Select ProName,ProPrice from Product where ProId = ?");
			ps.setInt(1, code);
			ResultSet rs = ps.executeQuery();

			PreparedStatement ps2 = connection
					.prepareStatement("insert into cart(CProName,CProPrice,PurchesQty)values(?,?,?)");

			while (rs.next()) {
				System.out.println("Selected Prodeuct : " + rs.getString(1) + "Rs." + rs.getString(2) + ".0/nos.");

				String S1 = rs.getString(1);
				String S2 = rs.getString(2);
				ps2.setString(1, S1);
				ps2.setString(2, S2);
				ps2.setInt(3, quantity);
				ps2.execute();
				System.out.println("Product add to cart Successfuly...");
			}

			addMoreProduct();

		} catch (Exception e) {
			e.getMessage();
		}
	}

	public static void getBill() {
		BillingStmt billingStmt = new BillingStmt();
		billingStmt.getBillDetails();
	}

	public static void addMoreProduct() throws ClassNotFoundException, SQLException {
		Scanner scanner = new Scanner(System.in);
		System.out.println("\nWant to add more product in cart?...(Y/N)");
		String input = scanner.next();
		for (int i = 0; i < input.length(); i++) {
			if (input.charAt(i) == 'Y' || input.charAt(i) == 'y') {
				Dashboard dashboard = new Dashboard();
				dashboard.getProductPurches();
			} else if (input.charAt(i) == 'N' || input.charAt(i) == 'n') {
				getBill();
			} else {
				System.out.println("Wrong input!...Please try again**");
				addMoreProduct();
			}
		}

	}
}

package com.velocity.admin.ecommerce;

/* Author name - Raghvendr mane & Shubham Chambhare
 * 
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class FillStock {

	public void getStockManeger() {
		String name = null;
		String price = null;
		int SaleQuantity;
		try {

			Database db = new Database();
			Connection connection = db.getConDetails();
			PreparedStatement preparedStatement = connection
					.prepareStatement("Select CProName,CProPrice,PurchesQty from cart");
			ResultSet rs = preparedStatement.executeQuery();// query for geting cart prodct list

			while (rs.next()) {

				name = rs.getString(1);
				price = rs.getString(2);
				SaleQuantity = rs.getInt(3);
				// Adding a cart list data into stock
				PreparedStatement pStatement = connection
						.prepareStatement("insert into stock(CProName,CProPrice,SaleQty)values(?,?,?)");
				pStatement.setString(1, name);
				pStatement.setString(2, price);
				pStatement.setInt(3, SaleQuantity);
				pStatement.execute();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
